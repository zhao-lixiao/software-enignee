create database db;
use db;
Create table  O
(
  Ono char(10) Primary Key,
  Onum char(5),
  CustomerName char(20) Unique,
  CustomerAddress char(20) ,
  CustomerPhone int ,
  CreateTime Date
);

Create table E
(
  EmployeeID char (10) Primary Key,
  EmployeeName char (20),
  EmployeeAge int
);

Create table T
(
  Tno char (10),
  Ono char(10) ,
  StartAddress char (20),
  EndAddress char (20),
  TransferCarNo char (10),
  NowOrderLocation char (20),
  ArriveTime date,
  Primary Key(Ono,Tno),
  Foreign Key (Ono) References O(Ono)

);

Insert 
Into O(Ono,Onum)
Values('0123','5');


Update O
set Onum = 4
Where Ono = '001';

Delete
From O
Where Ono = '002';




Create View User
AS 
Select *
From E;

Create Table Admin
(
AdminNO char(10)Primary Key,
AdminName char(10),
AdminPassword int
);

Create View Admin
As

Select *
From Admin;

Create Table Programmer
(
ProgrammerNo Char(10)Primary Key,
DataArrage Char(20),
DesignCache Char(20)
);